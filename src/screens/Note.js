import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import Typography from "@material-ui/core/Typography";
import {useParams} from 'react-router-dom';
import Paper from "@material-ui/core/Paper";
import {getNoteById} from "../redux/actions/todo";

const useStyles = makeStyles((theme) => ({
    content: {
        minHeight: '100vh',
        flexGrow: 1,
        padding: theme.spacing(6),
        backgroundColor: '#ffffff',
        color: '#000',
        wordBreak: 'break-all'
    },
    toolbar: {
        [theme.breakpoints.down('xs')]: theme.mixins.toolbar,
    },
    typography: {
        whiteSpace: 'pre-wrap'
    }
}));


const Note = ({ history }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {id} = useParams();
    const {note, notes} = useSelector(({todo}) => todo);

    useEffect(() => {
        dispatch(getNoteById(+id))
    }, [dispatch, id]);

    useEffect(() => {
       const find = notes.find(note => note.id === +id);

       if (find === undefined) {
           history.push('/notes')
       }
    }, [history, notes, id]);

    return (
        <Paper
            square
            elevation={0}
            className={classes.content}
        >
            <div className={classes.toolbar} />
            <Typography className={classes.typography}>
                {note?.body}
            </Typography>
        </Paper>
    );
};

export default Note;