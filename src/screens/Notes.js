import React from 'react';
import PaperComponent from "../components/PaperComponent";

const Notes = () => {
    return (
        <PaperComponent>
            Choose any note to see
        </PaperComponent>
    );
};

export default Notes;