import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import FabComponent from "../components/FabComponent";
import DrawerComponent from "../components/DrawerComponent";
import Routes from "../routes/Routes";
import DialogComponent from "../components/DialogComponent";
import AppBarComponent from "../components/AppBarComponent";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flex: 1
    }
}));

export default function Main() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBarComponent />
            <DrawerComponent />
            <FabComponent />
            <DialogComponent />
            <Routes />
        </div>
    );
}