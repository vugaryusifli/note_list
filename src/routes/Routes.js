import React, {lazy, Suspense} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import LoadingComponent from "../components/LoadingComponent";
import Error404Component from "../components/Error404Component";
const Notes = lazy(() => import("../screens/Notes"));
const Note = lazy(() => import("../screens/Note"));

const Routes = () => {
    return (
        <Suspense fallback={<LoadingComponent />}>
            <Switch>
                <Route exact path='/notes' component={Notes} />
                <Route exact path='/notes/:id' component={Note} />
                <Route path='/404' component={Error404Component} />
                <Redirect exact from="/" to="/notes" />
                <Redirect from='*' to='/404' />
            </Switch>
        </Suspense>
    );
};

export default Routes;