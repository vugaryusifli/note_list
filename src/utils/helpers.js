export const checkEmptyArr = (arr) => {
   return arr.every(item => item.deleted);
};

export const checkCountDeletedArr = (arr) => {
    const deletedStatus = arr.map(item => item.deleted === true);
    return deletedStatus.filter(item => item).length;
};