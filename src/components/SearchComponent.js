import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import InputBase from "@material-ui/core/InputBase";
import {useDispatch, useSelector} from "react-redux";
import {setSearchTitle} from "../redux/actions/todo";

const useStyles = makeStyles((theme) => ({
    search: {
        // borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        display: 'flex',
        alignItems: 'center',
        height: '5vh'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        textAlign: 'center',
        fontSize: 14,
        paddingRight: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        width: '100%'
    },
}));

const SearchComponent = () => {
    const dispatch = useDispatch();
    const classes  = useStyles();
    const searchTitle = useSelector(({ todo }) => todo.searchTitle);

    const handleSearchTitle = (e) => {
        dispatch(setSearchTitle(e.target.value));
    };

    return (
        <div className={classes.search}>
            <InputBase
                placeholder="Search everywhere"
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                value={searchTitle}
                inputProps={{ 'aria-label': 'search' }}
                onChange={handleSearchTitle}
            />
        </div>
    );
};

export default SearchComponent;