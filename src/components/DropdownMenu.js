import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import React from "react";
import {useDispatch} from "react-redux";
import {toggleDialog} from "../redux/actions/settings";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Typography from "@material-ui/core/Typography";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    icon: {
        minWidth: 35,
    }
}));

const DropdownMenu = ({note}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleOpenDialog = (key) => {
        dispatch(toggleDialog({note, key}));
        handleClose();
    };

    return (
        <>
            <IconButton
                edge="end"
                aria-controls="note-menu"
                aria-haspopup="true"
                onClick={handleClick}
                size="small"
            >
                <MoreVertIcon />
            </IconButton>
            <Menu
                id="note-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={() => handleOpenDialog('edit')}>
                    <ListItemIcon className={classes.icon}>
                        <EditIcon fontSize="small" />
                    </ListItemIcon>
                    <Typography noWrap>
                        Edit
                    </Typography>
                </MenuItem>
                <MenuItem onClick={() => handleOpenDialog('delete')}>
                    <ListItemIcon className={classes.icon}>
                        <DeleteIcon fontSize="small" />
                    </ListItemIcon>
                    <Typography noWrap>
                        Delete
                    </Typography>
                </MenuItem>
            </Menu>
        </>
    );
};

export default DropdownMenu;