import React, {useState} from 'react';
import _ from 'lodash';
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Drawer from "@material-ui/core/Drawer";
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import DrawerList from "./DrawerList";
import Hidden from "@material-ui/core/Hidden";
import {toggleDrawer} from "../redux/actions/settings";
import Typography from "@material-ui/core/Typography";
import {checkCountDeletedArr, checkEmptyArr} from "../utils/helpers";
import SearchComponent from "./SearchComponent";
import ScrollComponent from "./ScrollComponent";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        overflow: 'hidden',
        width: drawerWidth,
        backgroundColor: 'rgb(37, 37, 37)'
    },
}));

const DrawerComponent = (props) => {
    const { window } = props;
    const classes = useStyles();
    const dispatch = useDispatch();
    const [data, setData] = useState([]);
    const {notes} = useSelector(({todo}) => todo);
    const {open} = useSelector(({settings}) => settings.drawer);
    const searchTitle = useSelector(({ todo }) => todo.searchTitle);

    const handleDrawerToggle = () => {
        dispatch(toggleDrawer());
    };

    React.useEffect(() => {
        if (searchTitle.length !== 0) {
            setData(_.filter(notes, item => item.title.toLowerCase().includes(searchTitle.toLowerCase())));
        } else {
            setData(notes);
        }
    }, [notes, searchTitle]);

    const drawer = (
        <div>
            <SearchComponent />
            <Divider />
            <ScrollComponent maxHeight={'60vh'}>
                <List>
                    {checkEmptyArr(data) && (
                        <Typography noWrap style={{ textAlign: 'center' }}>
                            There are not any notes
                        </Typography>
                    )}
                    {data?.map(note => {
                        if (!note.deleted) {
                            return (
                                <DrawerList note={note} key={note.id} />
                            )
                        }
                        return null;
                    })}
                </List>
            </ScrollComponent>
            <Divider />
            <ScrollComponent maxHeight={'35vh'}>
                <List>
                    <Typography noWrap style={{ textAlign: 'center' }} gutterBottom>
                        {checkCountDeletedArr(data)} trashed note
                    </Typography>
                    {data?.map(note => {
                        if (note.deleted) {
                            return (
                                <DrawerList note={note} key={note.id} />
                            )
                        }
                        return null;
                    })}
                </List>
            </ScrollComponent>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <nav className={classes.drawer} aria-label="mailbox folders">
            <Hidden smUp implementation="css">
                <Drawer
                    container={container}
                    variant="temporary"
                    anchor={"left"}
                    open={open}
                    onClose={handleDrawerToggle}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true
                    }}
                >
                    {drawer}
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    {drawer}
                </Drawer>
            </Hidden>
        </nav>
    );
};

export default DrawerComponent;