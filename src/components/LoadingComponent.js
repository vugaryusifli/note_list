import React from 'react';
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: 99,
        position: 'relative',
        flexGrow: 1,
        minHeight: '100vh',
        color: '#fff'
    },
}));

const LoadingComponent = () => {
    const classes = useStyles();

    return (
        <Backdrop className={classes.backdrop} open>
            <CircularProgress color="inherit" />
        </Backdrop>
    );
};

export default LoadingComponent;