import React from 'react';
import Fab from "@material-ui/core/Fab";
import {makeStyles} from "@material-ui/core/styles";
import AddIcon from '@material-ui/icons/Add';
import {useDispatch} from "react-redux";
import {toggleDialog} from "../redux/actions/settings";

const useStyles = makeStyles((theme) => ({
    fab: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    }
}));

const FabComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const handleOpenModal = () => {
        dispatch(toggleDialog({note: null, key: 'add'}));
    };

    return (
        <Fab
            aria-label="Add"
            className={classes.fab}
            color="primary"
            onClick={handleOpenModal}
        >
            <AddIcon />
        </Fab>
    );
};

export default FabComponent;