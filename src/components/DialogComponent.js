import React, {useEffect, useState} from 'react';
import Dialog from "@material-ui/core/Dialog";
import {useDispatch, useSelector} from "react-redux";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {toggleDialog} from "../redux/actions/settings";
import {addNote, deleteNote, editNote} from "../redux/actions/todo";
import FormComponent from "./FormComponent";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    dialogAction: {
        padding: '8px 24px 16px'
    }
}));

const initialState = {
    title: '',
    body: ''
};

const DialogComponent = (props) => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const {open, action, data} = useSelector(({ settings }) => settings.dialog);
    const [note, setNote] = useState(initialState);

    const handleChangeForm = (e) => {
        const {name, value} = e.target;

        setNote(_note => ({
            ...note,
            [name]: value
        }));
    };

    useEffect(() => {
        if (data) {
            setNote(data);
        } else {
            setNote(initialState)
        }
    }, [data]);

    const handleDisagreeDialog = () => {
        dispatch(toggleDialog({note, key: action}))
    };

    const handleAddNote = () => {
        const data = {
            ...note,
            id: new Date().getTime() + Math.random()
        };

        history.push(`/notes/${data.id}`);

        dispatch(addNote(data));
    };

    const handleEditNote = () => {
        dispatch(editNote(note));
    };

    const handleDeleteNote = () => {
        dispatch(deleteNote(note.id));
    };

    const handleAgreeDialog = () => {
        if (action === 'edit') {
            handleEditNote();
        } else if (action === 'delete') {
            handleDeleteNote();
        } else {
            handleAddNote();
        }
        handleDisagreeDialog();
        setNote(initialState);
    };

    return (
        <Dialog
            fullWidth
            maxWidth="sm"
            open={open}
            onClose={handleDisagreeDialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            style={{ zIndex: 9999 }}
        >
            <DialogTitle id="alert-dialog-title">{action.toUpperCase()}</DialogTitle>
            <DialogContent>
                {action === 'delete'
                    ? (
                        <DialogContentText id="alert-dialog-description">Are you sure you want to delete this note {data.id}?</DialogContentText>
                    )
                    : (
                        <FormComponent note={note} handleChangeForm={handleChangeForm}/>
                    )}
            </DialogContent>
            <DialogActions className={classes.dialogAction}>
                <Button onClick={handleDisagreeDialog} color="secondary">
                    Close
                </Button>
                <Button
                    onClick={handleAgreeDialog}
                    variant="contained"
                    color="primary"
                >
                    {action}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default DialogComponent;