import React, {useEffect} from 'react';
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper: ({ maxHeight }) => ({
        overflow: 'auto',
        maxHeight,
        minHeight: maxHeight,
        backgroundColor: 'rgb(37, 37, 37)',
    })
}));

const ScrollComponent = ({ children, maxHeight, className }) => {
    const classes = useStyles({maxHeight});
    const ref = React.createRef();

    useEffect(() => {
       const scrollToMyRef = () => {
           const scroll = ref.current.scrollHeight - ref.current.clientHeight;
           ref.current.scrollTo(0, scroll);
        };

        scrollToMyRef();
    }, [ref]);

    return (
        <Paper
            ref={ref}
            square
            className={[classes.paper, className ? className : ''].join(' ')}
            elevation={0}
        >
            {children}
        </Paper>
    );
};

export default ScrollComponent;