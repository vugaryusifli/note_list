import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#ffffff',
        color: '#000000'
    }
}));

const PaperComponent = ({children}) => {
    const classes = useStyles();

    return (
        <Paper elevation={0} square className={classes.root}>
            {children}
        </Paper>
    );
};

export default PaperComponent;