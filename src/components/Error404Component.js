import React from 'react';
import PaperComponent from "./PaperComponent";

const Error404Component = () => {
    return (
        <PaperComponent>
            Page not found :(
        </PaperComponent>
    );
};

export default Error404Component;