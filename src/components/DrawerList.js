import React from "react";
import {NavLink} from "react-router-dom";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItem from "@material-ui/core/ListItem";
import DropdownMenu from "./DropdownMenu";
import Typography from "@material-ui/core/Typography";
import {deleteNote} from "../redux/actions/todo";
import {useDispatch} from "react-redux";
import UndoIcon from '@material-ui/icons/Undo';
import IconButton from "@material-ui/core/IconButton";

const DrawerList = ({note}) => {
    const dispatch = useDispatch();

    const handleUndoDeletedNote = () => {
        dispatch(deleteNote(note.id));
    };

    return (
        <ListItem
            button
            component={NavLink}
            exact
            to={`/notes/${note.id}`}
        >
            <ListItemText primary={
                <Typography noWrap>
                    {note.title}
                </Typography>
            }/>
            <ListItemSecondaryAction>
                {!note.deleted ? (
                    <DropdownMenu note={note}/>
                ) : (
                    <IconButton
                        edge="end"
                        size="small"
                        onClick={handleUndoDeletedNote}
                    >
                        <UndoIcon />
                    </IconButton>
                )}
            </ListItemSecondaryAction>
        </ListItem>
    )
};

export default DrawerList;