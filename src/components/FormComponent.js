import React from 'react';
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    textarea: {
        minHeight: 100
    },
    field: {
        marginBottom: 12
    }
}));

const FormComponent = ({note, handleChangeForm}) => {
    const classes  = useStyles();

    return (
       <form noValidate autoComplete="off">
           <div className={classes.field}>
               <TextField
                   id="filled-basic"
                   label="Title"
                   variant="outlined"
                   fullWidth
                   autoFocus
                   name="title"
                   onChange={handleChangeForm}
                   value={note.title}
               />
           </div>
           <div>
               <TextField
                   id="outlined-textarea"
                   label="Body"
                   fullWidth
                   multiline
                   name="body"
                   variant="outlined"
                   value={note.body}
                   onChange={handleChangeForm}
                   inputProps={{ className: classes.textarea }}
               />
           </div>
       </form>
    );
};

export default FormComponent;