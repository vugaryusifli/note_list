import thunk from 'redux-thunk';
import rootReducers from "./reducers/rootReducers";
import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
    const { logger } = require(`redux-logger`);

    middlewares.push(logger);
}

const store = createStore(
    rootReducers,
    composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;