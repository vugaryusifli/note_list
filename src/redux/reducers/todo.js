import * as types from '../types';

const initialState = {
    notes: [
        {id: 1,title: 'Untitled', body: 'Untitled', deleted: false},
        {id: 2,title: 'test', body: 'test', deleted: false},
        {id: 3,title: 'Hey', body: 'Hey', deleted: false},
        {id: 4,title: 'SADDDDDDDDDDDDDDDDDDD', body: 'Untitled', deleted: false},
        {id: 5,title: 'Untitled', body: 'Untitled', deleted: false},
        {id: 6,title: 'Untitled', body: 'Hey', deleted: false},
        {id: 7,title: 'Untitled', body: 'Untitled', deleted: false},
        {id: 8,title: 'Untitled', body: 'Untitled', deleted: false},
        {id: 9,title: 'Hey', body: 'Hey', deleted: false},
    ],
    note: null,
    searchTitle: ''
};

export default function todo(state = initialState, action) {
    switch (action.type) {
        case types.ADD_NOTE:
            return {
               ...state,
               notes: [
                   ...state.notes,
                   {
                       ...action.note,
                       title: !action.note.title ? 'Untitled' : action.note.title,
                       deleted: false
                   }
               ]
            };
        case types.EDIT_NOTE:
            const {id, title, body} = action.note;
            const editNote = state.notes.map(note => note.id === id ? {...note, title, body} : note);

            return {
                ...state,
                notes: editNote,
                note: action.note
            };
        case types.DELETE_NOTE:
            const notes = state.notes.map(note => note.id === action.id ? {...note, deleted: !note.deleted} : note);

            return {
                ...state,
                notes
            };
        case types.GET_NOTE_BY_ID:
            const noteById = state.notes.find(note => note.id === action.id);

            return {
              ...state,
              note: noteById
            };
        case types.SET_SEARCH_TITLE:
            return {
                ...state,
                searchTitle: action.title
            };
        default:
            return state;
    }
}




// import * as types from '../types';
//
// const initialState = {
//     notes: [
//         {id: 1,title: 'Vugar', body: 'Vugar', deleted: false},
//         {id: 2,title: 'Dinara', body: 'Dinara', deleted: false},
//         {id: 3,title: 'Hey', body: 'Hey', deleted: false},
//         {id: 4,title: 'Vugar', body: 'Vugar', deleted: false},
//         {id: 5,title: 'Dinara', body: 'Dinara', deleted: false},
//         {id: 6,title: 'Hey', body: 'Hey', deleted: false},
//     ],
//     note: null,
//     deletedNotes: []
// };


// export default function todo(state = initialState, action) {
//     switch (action.type) {
//         case types.ADD_NOTE:
//             return {
//                 ...state,
//                 notes: [
//                     ...state.notes,
//                     action.note
//                 ]
//             };
//         case types.EDIT_NOTE:
//             const {id, title, body} = action.note;
//             const editNote = state.notes.map(note => note.id === id ? {...note, title, body} : note);
//
//             return {
//                 ...state,
//                 notes: editNote,
//                 note: action.note
//             };
//         case types.DELETE_NOTE:
//             const notes = state.notes.map(note => note.id === action.id ? {...note, deleted: !note.deleted} : note);
//             const note = state.notes.find(note => note.id === action.id);
//             const deletedNotes = state.deletedNotes.concat(note);
//
//             return {
//                 ...state,
//                 notes,
//                 deletedNotes
//             };
//         case types.DELETE_ALL_NOTES:
//             return {
//
//             };
//         case types.GET_NOTE_BY_ID:
//             const noteById = state.notes.find(note => note.id === action.id);
//
//             return {
//                 ...state,
//                 note: noteById
//             };
//         default:
//             return state;
//     }
// }