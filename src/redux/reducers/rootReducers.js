import {combineReducers} from "redux";
import todo from "./todo";
import settings from "./settings";

export default combineReducers({
    todo,
    settings
});