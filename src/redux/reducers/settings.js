import * as types from "../types";

const initialState = {
    drawer: {
        open: false
    },
    dialog: {
        action: '',
        open: false,
        data: null
    }
};

export default function settings(state = initialState, action) {
    switch (action.type) {
        case types.TOGGLE_DRAWER:
            return {
                ...state,
                drawer: {
                    open: !state.drawer.open
                }
            };
        case types.TOGGLE_DIALOG:
            return {
                ...state,
                dialog: {
                    open: !state.dialog.open,
                    action: action.payload.key,
                    data: action.payload.note
                }
            };
        default:
            return state;
    }
}