import * as types from '../types';

export const getNoteById = (id) => dispatch => {
    dispatch(getNoteByIdAction(id));
};

export const getNoteByIdAction = (id) => ({
    type: types.GET_NOTE_BY_ID,
    id
});

export const addNote = (note) => dispatch => {
    dispatch(addNoteAction(note));
};

const addNoteAction = (note) => ({
    type: types.ADD_NOTE,
    note
});

export const editNote = (note) => dispatch => {
    dispatch(editNoteAction(note));
};

const editNoteAction = (note) => ({
    type: types.EDIT_NOTE,
    note
});

export const deleteNote = (id) => dispatch => {
    dispatch(deleteNoteAction(id));
};

const deleteNoteAction = (id) => ({
    type: types.DELETE_NOTE,
    id
});

export const setSearchTitle = (title) => dispatch => {
    dispatch(setSearchTitleAction(title));
};

const setSearchTitleAction = (title) => ({
    type: types.SET_SEARCH_TITLE,
    title
});