import * as types from "../types";

export const toggleDrawer = () => ({
    type: types.TOGGLE_DRAWER
});

export const toggleDialog = (data) => ({
    type: types.TOGGLE_DIALOG,
    payload: data
});