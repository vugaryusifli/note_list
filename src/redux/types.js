export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';
export const TOGGLE_THEME = 'TOGGLE_THEME';
export const TOGGLE_DIALOG = 'TOGGLE_DIALOG';
export const ADD_NOTE = 'ADD_NOTE';
export const DELETE_NOTE = 'DELETE_NOTE';
export const SET_SEARCH_TITLE = 'SET_SEARCH_TITLE';
export const EDIT_NOTE = 'EDIT_NOTE';
export const GET_NOTE_BY_ID = 'GET_NOTE_BY_ID';
