import React from "react";
import Main from "./screens/Main";
import {BrowserRouter as Router} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import {ThemeProvider} from "@material-ui/styles";
import {createMuiTheme} from "@material-ui/core/styles";

const theme = createMuiTheme({
    palette: {
        type: 'dark'
    }
});

const App = () => {
    return (
        <Router>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <Main/>
                </ThemeProvider>
            </Provider>
        </Router>
    );
};

export default App;
